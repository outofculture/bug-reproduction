import React from "react"
import {View, Text} from "react-native"
import {NativeRouter, Route, Switch} from "react-router-native"
import {Loading} from "./src/components/loading"
import {styles} from "./src/styles"
import {Home} from "./src/components/home"
import {Intro} from "./src/components/intro";

class ErrorBoundary extends React.Component {
  constructor(props) {
     super(props);
     this.state = { hasError: false };
   }

   static getDerivedStateFromError(error) {
     // Update state so the next render will show the fallback UI.
     return { hasError: true };
   }

   componentDidCatch(error, info) {
     // You can also log the error to an error reporting service
     console.error(error, info);
   }

   render() {
     if (this.state.hasError) {
       // You can render any custom fallback UI
       return <Text>Something went wrong.</Text>;
     }

     return this.props.children;
   }
}


export default class App extends React.Component {
  render() {
    return (
      <ErrorBoundary>
        <NativeRouter>
          <View style={styles.container}>
            <Switch>
              <Route exact path="/" component={Loading} />
              <Route path="/home" component={Home} />
              <Route path="/intro" component={Intro} />
            </Switch>
          </View>
        </NativeRouter>
      </ErrorBoundary>
    )
  }

  componentDidMount() {
    // check memory for api key
    // if no api key, take user to intro screen
    // else take user to home
  }
}

