import {Text, View} from "react-native"
import {Link} from "react-router-native"
import React from "react"

export const Loading = () => <View><Text>Loading... </Text><Link to='/home'><Text>Go Home</Text></Link></View>;
