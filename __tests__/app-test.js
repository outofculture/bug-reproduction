import React from 'react'
import App from "../App"
import {MemoryRouter} from 'react-router-native'
import renderer from 'react-test-renderer'
import {Home} from "../src/components/home"
import {Intro} from "../src/components/intro"

describe('App', () => {
  describe('without a stored api key', () => {
    let app
    beforeEach(() => {
      app = renderer.create(
        <MemoryRouter>
          <App/>
        </MemoryRouter>
      )
    })
    it('looks the same as our snapshot', () => {
      expect(app).toMatchSnapshot()
    })
    it('loads Intro', () => {
      expect(app.root.findByType(Intro)).toBeDefined()
      expect(app.root.findByType(Home)).not.toBeDefined()
    })
  })
  describe('with a stored api key', () => {
    let app
    beforeEach(() => {
      // TODO how do we store an api key?
      app = renderer.create(
        <MemoryRouter>
          <App />
        </MemoryRouter>
      )
    })
    it('looks the same as our snapshot', () => {
      expect(app).toMatchSnapshot()
    })
    it('loads Home', () => {
      expect(app.root.findByType(Intro)).not.toBeDefined()
      expect(app.root.findByType(Home)).toBeDefined()
    })
  })
})
